import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс производит удаление коменнтариев, двойных и более пробелов, заменяет имена переменных, класса на набор символов.
 */
public class Obfuscator implements Regular {
    private String code;
    private String pathFile;
    private String pathEnd;
    private String nameFile;
    private String newName;

    BD bd = new BD();

    Obfuscator() {
    }

    /**
     * @param pathFile - путь к файлу который нужно изменить
     */
    Obfuscator(String pathFile, String pathEnd) {
        this.pathFile = pathFile;
        this.pathEnd = pathEnd;
    }

    /**
     * Осуществляет запуск методов
     */
    void launch() throws SQLException {
        code = ReadFile.read(pathFile);
        deleteElements(COMMENTS);
        deleteElements(SPACES);
        renameClass();
        replacementIdentifiers(serchIdentifiers(NAMEVARIABLES));
        replacementIdentifiers(serchIdentifiers(NAMEMETHODS));
        WriteFile.write(code, pathFile);
        bd.write(nameFile, newName);
    }

    /**
     * Осуществляет удаление элементов кода по регулярке
     *
     * @param regex - регулярка для удаления
     */
    public void deleteElements(String regex) {
        Pattern pattern = Pattern.compile(regex);
        code = code.replaceAll(String.valueOf(pattern), "");
    }

    /**
     * Осуществляет поиск имени класса и заменят его на другое
     */
    public void renameClass() {
        Pattern pattern = Pattern.compile(CLASS);
        Matcher matcher = pattern.matcher(code);
        String nameClass = "";
        while (matcher.find()) {
            nameClass = matcher.group();
            nameFile = nameClass;
        }
        newName = randomWord();
        code = code.replaceAll(nameClass, newName);
        pathFile = pathEnd + newName + ".java";
    }

    /**
     * Осуществляет рандом слов из 3 букв английского языка
     *
     * @return - рандомное слово
     */
    private String randomWord() {
        StringBuilder word = new StringBuilder();

        char[] smallLettersAlphabet = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};

        for (int i = 0; i < 3; i++) {
            Random random = new Random();
            word.append(smallLettersAlphabet[random.nextInt(26)]);
        }
        return word.toString();
    }


    /**
     * Осуществляет поиск идентификаторов в коде и записывает их в arraylist
     */
    public ArrayList<String> serchIdentifiers(String regex) {
        ArrayList<String> nameIdentifiers = new ArrayList<>();
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(code);
        String name;
        while (matcher.find()) {
            name = matcher.group();
            nameIdentifiers.add(name);
        }
        nameIdentifiers = new ArrayList<String>(new HashSet<String>(nameIdentifiers));
        return nameIdentifiers;
    }

    /**
     * Заменяет идентификаторы в коде на новые
     */
    public void replacementIdentifiers(ArrayList<String> nameIndentifiers) {
        for (String nameVariable : nameIndentifiers) {
            String name = randomWord();
            code = code.replaceAll("\\b" + nameVariable + "\\b", " " + name);
        }
    }

    public void setCode(String text) {
        code = text;
    }

    public String getCode() {
        return code;
    }
}
