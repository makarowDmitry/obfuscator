import com.mysql.cj.jdbc.result.ResultSetMetaData;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.MapValueFactory;

import java.sql.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class BD implements Regular {
    Connection conn;
    Statement statement;
    private int columnCount;

    @FXML
    private TableView<HashMap> tableView;

    @FXML
    private void initialize() {
        try {
            showDataBaseInfo();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void write(String nameFile, String newName) throws SQLException {
        Date date = new Date();
        String today = date.toString();
        connect();
        String sql = "INSERT INTO Journal (dateObfuscation,NameFile,NewFile) VALUES ('" + today + "', '" + nameFile + "', '" + newName + "');";
        statement.executeUpdate(sql);
        close();

    }

    private void connect() throws  SQLException{
        conn = DriverManager.getConnection("jdbc:mysql://localhost/obfuscator?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "mysql", "mysql");
        statement = conn.createStatement();
    }
    private void close() throws SQLException {
        statement.close();
        conn.close();
    }

    public ResultSet request(String query) throws SQLException {
        return statement.executeQuery(query);
    }

    public int countColumns(String tableName) throws SQLException {
        ResultSetMetaData rsm = (ResultSetMetaData) request("SELECT * FROM `" + tableName + "`").getMetaData();
        return rsm.getColumnCount();
    }

    public void showDataBaseInfo() throws SQLException {
        connect();
        columnCount = countColumns(TABLENAME);

        ArrayList<String> columnNames = getColumnNames(TABLENAME, columnCount);
        ArrayList<TableColumn> tableColumns = createTableColumns(columnNames);

        setItemsInTable(columnNames);
        setValuesColumns(tableColumns, columnNames);
        addColumnsInTable(tableColumns);

        close();
    }

    private void setValuesColumns(ArrayList<TableColumn> tableColumns, ArrayList<String> columnNames) {
        for (int i = 0; i < columnCount; i++) {
            tableColumns.get(i).setCellValueFactory(new

                    MapValueFactory<String>(columnNames.get(i)));
        }
    }

    private void addColumnsInTable(ArrayList<TableColumn> tableColumns) {
        for (int i = 0; i < columnCount; i++) {
            tableView.getColumns().add(tableColumns.get(i));
        }
    }

    private ArrayList<String> getColumnNames(String tableName, int columnCount) throws SQLException {
        ArrayList<String> columnsNames = new ArrayList<>();
        ResultSetMetaData rsm = (ResultSetMetaData) request("SELECT * FROM `" + tableName + "`").getMetaData();
        for (int i = 1; i <= columnCount; i++) {
            columnsNames.add(rsm.getColumnName(i));
        }

        return columnsNames;
    }

    private ArrayList<TableColumn> createTableColumns(ArrayList<String> columnNames) {
        ArrayList<TableColumn> tableColumns = new ArrayList<>();
        for (int i = 0; i < columnCount; i++) {
            if (i == 1) {
                TableColumn tableColumn = new TableColumn(columnNames.get(i));
                tableColumn.setStyle("-fx-alignment: CENTER-LEFT;");
                tableColumns.add(tableColumn);

            } else {
                TableColumn tableColumn = new TableColumn(columnNames.get(i));
                tableColumn.setStyle("-fx-alignment: CENTER;");
                tableColumns.add(tableColumn);
            }
        }
        return tableColumns;
    }

    private void setItemsInTable(ArrayList<String> columnNames) throws SQLException {
        tableView.setItems(getInfo(TABLENAME, columnCount, columnNames));
    }

    public ObservableList<HashMap> getInfo(String tableName, int columnCount, ArrayList<String> columnsNames) throws SQLException {
        return FXCollections.observableArrayList(readData(tableName, columnCount, columnsNames));
    }

    private ArrayList<HashMap> readData(String tableName, int columnCount, ArrayList<String> columnNames) throws SQLException {
        ArrayList<HashMap> hashMaps = new ArrayList<>();
        ResultSet rs = request("SELECT * FROM `" + tableName + "`");
        while (rs.next()) {
            HashMap<String, String> dataBaseInfo = new HashMap<>();
            for (int i = 1; i <= columnCount; i++) {
                dataBaseInfo.put(columnNames.get(i - 1), rs.getString(i));
            }
            hashMaps.add(dataBaseInfo);
        }
        rs.close();

        return hashMaps;
    }
}
