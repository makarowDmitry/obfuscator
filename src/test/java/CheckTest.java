import org.junit.Test;

import static org.junit.Assert.*;

public class CheckTest {
Check check = new Check();
    @Test
    public void checkPathTrueOne() {
        String path = "src/test.java";
        String pathEnd = "src/";
        assertTrue(check.checkPath(path,pathEnd));
    }
    @Test
    public void checkPathOneFalse() {
        String path = "src/test.jav";
        String pathEnd = "src/";
        assertFalse(check.checkPath(path, pathEnd));
    }
    @Test
    public void checkPathTwoFalse() {
        String path = "src/test.java";
        String pathEnd = "src/34";
        assertFalse(check.checkPath(path,pathEnd));
    }
    @Test
    public void checkPathTrueTwo() {
        String path = "src/test.java";
        String pathEnd = "src\\";
        assertTrue(check.checkPath(path,pathEnd));
    }
}