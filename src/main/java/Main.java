import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application{
    public static void main(String[] args)   {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("show.fxml"));

        primaryStage.setTitle("Обфускатор");
        primaryStage.setScene(new Scene(root, 300, 350));
        primaryStage.show();
    }
}
