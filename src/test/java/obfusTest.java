import org.junit.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertFalse;

public class obfusTest {
    Obfuscator obfus = new Obfuscator();

    private void setCode() {
        obfus.setCode("package file;\n" +
                "\n" +
                "public class Test {\n" +
                "    public static void main(String[] args) {\n" +
                "        int num;\n" +
                "        String key=\"45\";\n" +
                "        double num2;\n" +
                "        char ch3;\n" +
                "        calcul();\n" +
                "        //Без труда\n" +
                "    }\n" +
                "\n" +
                "    public static int calcul(){\n" +
                "        return 4;\n" +
                "    }\n" +
                "}\n");
    }

    @Test
    public void checkNameVariables() {
        boolean check = false;
        setCode();
        obfus.replacementIdentifiers(obfus.serchIdentifiers(Regular.NAMEVARIABLES));
        String code = obfus.getCode();
        Pattern pattern = Pattern.compile("num|key|num2|ch3");
        Matcher matcher = pattern.matcher(code);
        while (matcher.find()) {
            check = true;
        }
        assertFalse(check);
    }

    @Test
    public void checkNameClass() {
        boolean check = false;
        setCode();
        obfus.renameClass();
        String code = obfus.getCode();
        Pattern pattern = Pattern.compile("Test");
        Matcher matcher = pattern.matcher(code);
        while (matcher.find()) {
            check = true;
        }
        assertFalse(check);

    }

    @Test
    public void checkNameFunc() {
        boolean check = false;
        setCode();
        obfus.replacementIdentifiers(obfus.serchIdentifiers(Regular.NAMEMETHODS));
        String code = obfus.getCode();
        Pattern pattern = Pattern.compile("calcul");
        Matcher matcher = pattern.matcher(code);
        while (matcher.find()) {
            check = true;
        }
        assertFalse(check);

    }

    @Test
    public void checkComments() {
        boolean check = false;
        setCode();
        obfus.deleteElements(Regular.COMMENTS);
        String code = obfus.getCode();
        Pattern pattern = Pattern.compile("\\\\");
        Matcher matcher = pattern.matcher(code);
        while (matcher.find()) {
            check = true;
        }
        assertFalse(check);

    }

}
