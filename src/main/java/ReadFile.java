import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс осуществляет чтение файла в одну строку
 */
class ReadFile {
    /**
     * Осуществляет чтение файла в одну строку
     *
     * @param url - путь к файлу
     * @return - код записанный в одну строку
     */
    static String read(String url) {
        StringBuilder str = new StringBuilder();

        try (BufferedReader bf = new BufferedReader(new FileReader(url))) {
            String line;
            while ((line = bf.readLine()) != null) {
                if (singleLineCommentCheck(line)) {
                    str.append(line);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return str.toString();
    }

    /**
     * Осуществляет проверку строки на однострочный комментарий
     *
     * @param line - строка для проверки
     * @return - true или false
     */
    private static boolean singleLineCommentCheck(String line) {
        boolean permission = true;
        Pattern pattern = Pattern.compile("//[^\\r\\n]+");
        Matcher matcher = pattern.matcher(line);
        if (matcher.find()) {
            permission = false;
        }

        return permission;
    }
}
