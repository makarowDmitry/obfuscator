import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Класс осуществляет запись данных на файл
 */
class WriteFile {
    /**
     * Осуществляет запись данных на файл
     *
     * @param str - текс который нужно записать
     * @param url - путь к файлу
     */
    static void write(String str, String url) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(url))) {
            bw.write(str);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
