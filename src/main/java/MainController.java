import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;


public class MainController {
    Check check = new Check();
    BD bd = new BD();

    @FXML
    private TextField pathFile;
    @FXML
    private TextField newPathFile;
    @FXML
    private Label error;

    @FXML
    private void click() throws SQLException {
        String path = pathFile.getText();
        String pathEnd = newPathFile.getText();
        File file = new File(path);
        if(check.checkPath(path,pathEnd) && file.exists()) {
            Obfuscator obfuscator = new Obfuscator(path, pathEnd);
            obfuscator.launch();
            error.setText("");
        }else {
            error.setText("Путь введен не верно");
        }
    }

    @FXML
    private void openJournal() throws IOException, SQLException {
        Parent root = FXMLLoader.load(getClass().getResource("dataBaseEditor.fxml"));
        Stage stage = new Stage();
        stage.setScene(new Scene(root,800, 800));
        stage.setTitle("Журнал");
        stage.show();
    }
}
